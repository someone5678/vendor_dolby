LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
APKEDITOR := prebuilts/tools-parasite/common/bin/APKEditor
LOCAL_MODULE := DaxUICustom
LOCAL_MODULE_TAGS := optional
ifeq ($(TARGET_CUSTOM_DOLBY_NO_LAUNCHER),true)
$(shell $(APKEDITOR) b -i $(LOCAL_PATH)/DaxUI -f -o $(LOCAL_PATH)/DaxUICustom.apk)
else
$(shell cp -rf $(LOCAL_PATH)/DaxUI/AndroidManifest.xml $(LOCAL_PATH)/AndroidManifest.xml.bak)
$(shell sed -i '/android.intent.category.LAUNCHER/d' $(LOCAL_PATH)/DaxUI/AndroidManifest.xml)
$(shell $(APKEDITOR) b -i $(LOCAL_PATH)/DaxUI -f -o $(LOCAL_PATH)/DaxUICustom.apk)
$(shell rm $(LOCAL_PATH)/DaxUI/AndroidManifest.xml)
$(shell mv AndroidManifest.xml.bak $(LOCAL_PATH)/DaxUI/AndroidManifest.xml)
endif
LOCAL_SRC_FILES := DaxUICustom.apk
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_CLASS := APPS
LOCAL_INSTALLED_MODULE_STEM := DaxUI.apk
LOCAL_SYSTEM_EXT_MODULE := true
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_RELATIVE_PATH := DaxUI
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_NO_STANDARD_LIBRARIES := true
LOCAL_OVERRIDES_PACKAGES := DaxUI AudioFX MusicFX
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)
