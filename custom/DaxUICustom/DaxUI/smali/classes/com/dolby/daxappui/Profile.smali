.class Lcom/dolby/daxappui/Profile;
.super Ljava/lang/Object;
.source "Profile.java"


# instance fields
.field private mIconNormal:I

.field private mIconSelected:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/dolby/daxappui/Profile;->mIconSelected:I

    .line 20
    iput p2, p0, Lcom/dolby/daxappui/Profile;->mIconNormal:I

    return-void
.end method


# virtual methods
.method getIcon(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 24
    iget p0, p0, Lcom/dolby/daxappui/Profile;->mIconSelected:I

    goto :goto_0

    :cond_0
    iget p0, p0, Lcom/dolby/daxappui/Profile;->mIconNormal:I

    :goto_0
    return p0
.end method
