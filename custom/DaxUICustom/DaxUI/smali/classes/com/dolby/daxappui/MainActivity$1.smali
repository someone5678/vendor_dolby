.class Lcom/dolby/daxappui/MainActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dolby/daxappui/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dolby/daxappui/MainActivity;


# direct methods
.method constructor <init>(Lcom/dolby/daxappui/MainActivity;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const-string p1, "MainActivity"

    .line 134
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDolbyIntentReceiver, action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.dolby.intent.action.DAP_PARAMS_UPDATE"

    .line 137
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_9

    const-string v0, "event name"

    .line 138
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    .line 139
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v3, "profile_setting_change"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_1
    const-string v3, "reset_profile_setting"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v5

    goto :goto_0

    :sswitch_2
    const-string v3, "ds_state_change"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v4

    goto :goto_0

    :sswitch_3
    const-string v3, "profile_change"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    move v1, v6

    :cond_0
    :goto_0
    const-string v0, "profile index is out of 0~3"

    const/4 v3, 0x4

    const-string v7, "Integer Value"

    if-eqz v1, :cond_6

    if-eq v1, v6, :cond_6

    if-eq v1, v5, :cond_3

    if-eq v1, v4, :cond_1

    goto/16 :goto_3

    .line 162
    :cond_1
    :try_start_1
    invoke-virtual {p2, v7, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    if-lez p2, :cond_2

    move v2, v6

    .line 163
    :cond_2
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-virtual {p0, v2}, Lcom/dolby/daxappui/MainActivity;->dsPowerChanged(Z)V

    goto/16 :goto_3

    .line 153
    :cond_3
    invoke-virtual {p2, v7, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    if-ltz p2, :cond_5

    if-lt p2, v3, :cond_4

    goto :goto_1

    .line 157
    :cond_4
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-virtual {p0, p2}, Lcom/dolby/daxappui/MainActivity;->resetProfile(I)V

    goto :goto_3

    .line 155
    :cond_5
    :goto_1
    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 142
    :cond_6
    invoke-virtual {p2, v7, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    .line 143
    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {v1}, Lcom/dolby/daxappui/MainActivity;->access$000(Lcom/dolby/daxappui/MainActivity;)Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {v1}, Lcom/dolby/daxappui/MainActivity;->access$000(Lcom/dolby/daxappui/MainActivity;)Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result v1

    if-ne v1, p2, :cond_b

    if-ltz p2, :cond_8

    if-lt p2, v3, :cond_7

    goto :goto_2

    .line 147
    :cond_7
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p0, p2}, Lcom/dolby/daxappui/MainActivity;->access$100(Lcom/dolby/daxappui/MainActivity;I)V

    goto :goto_3

    .line 145
    :cond_8
    :goto_2
    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_9
    const-string p2, "audio_server_restarted"

    .line 167
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_b

    .line 168
    iget-object p2, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p2}, Lcom/dolby/daxappui/MainActivity;->access$000(Lcom/dolby/daxappui/MainActivity;)Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p2

    if-eqz p2, :cond_a

    .line 169
    iget-object p2, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    invoke-static {p2}, Lcom/dolby/daxappui/MainActivity;->access$000(Lcom/dolby/daxappui/MainActivity;)Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/media/audiofx/AudioEffect;->release()V

    .line 170
    iget-object p0, p0, Lcom/dolby/daxappui/MainActivity$1;->this$0:Lcom/dolby/daxappui/MainActivity;

    new-instance p2, Lcom/dolby/dax/DolbyAudioEffect;

    invoke-direct {p2, v2, v2}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    invoke-static {p0, p2}, Lcom/dolby/daxappui/MainActivity;->access$002(Lcom/dolby/daxappui/MainActivity;Lcom/dolby/dax/DolbyAudioEffect;)Lcom/dolby/dax/DolbyAudioEffect;

    :cond_a
    const-string p0, "Dax effect recreate successfully"

    .line 172
    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception p0

    const-string p2, "Exception found in MainActivity::onReceive()"

    .line 175
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_b
    :goto_3
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x445cf8fa -> :sswitch_3
        0xa58b4ce -> :sswitch_2
        0x353a7aca -> :sswitch_1
        0x5e0085d5 -> :sswitch_0
    .end sparse-switch
.end method
