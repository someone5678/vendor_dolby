.class Lcom/dolby/daxappui/FragProfilePanelPageAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "FragProfilePanelPageAdapter.java"


# instance fields
.field private mFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/dolby/daxappui/FragProfilePanel;",
            ">;"
        }
    .end annotation
.end field

.field private final mProfileNames:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .line 27
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 23
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mFragments:Ljava/util/List;

    .line 28
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object p1

    invoke-virtual {p1}, Lcom/dolby/daxappui/DAXApplication;->getProfileNames()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mProfileNames:[Ljava/lang/String;

    const/4 p1, 0x0

    .line 30
    :goto_0
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mProfileNames:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 31
    invoke-static {p1}, Lcom/dolby/daxappui/FragProfilePanel;->newInstance(I)Lcom/dolby/daxappui/FragProfilePanel;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mProfileNames:[Ljava/lang/String;

    array-length p0, p0

    return p0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 58
    :cond_0
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mFragments:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/support/v4/app/Fragment;

    return-object p0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 0

    const/4 p0, -0x2

    return p0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanelPageAdapter;->mProfileNames:[Ljava/lang/String;

    aget-object p0, p0, p1

    return-object p0
.end method
