.class public Lcom/dolby/daxappui/exploreDolby/FragExploreDolbyAtmos;
.super Landroid/support/v4/app/Fragment;
.source "FragExploreDolbyAtmos.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCreateView$0$FragExploreDolbyAtmos(Landroid/view/View;)V
    .locals 2

    const-string p1, "http://www.dolby.com/us/en/technologies/dolby-atmos.html"

    .line 44
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 47
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 50
    invoke-virtual {p0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 38
    invoke-super {p0, p3}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const p3, 0x7f0c002e    # @layout/explore_dolby_atmos 'res/layout/explore_dolby_atmos.xml'

    const/4 v0, 0x0

    .line 40
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f090044    # @id/btnupgotodolbyatmos

    .line 42
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/RelativeLayout;

    .line 43
    new-instance p3, Lcom/dolby/daxappui/exploreDolby/-$$Lambda$FragExploreDolbyAtmos$RqQz2rH-RbGoQN7YJpcmN27Jm1s;

    invoke-direct {p3, p0}, Lcom/dolby/daxappui/exploreDolby/-$$Lambda$FragExploreDolbyAtmos$RqQz2rH-RbGoQN7YJpcmN27Jm1s;-><init>(Lcom/dolby/daxappui/exploreDolby/FragExploreDolbyAtmos;)V

    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method
