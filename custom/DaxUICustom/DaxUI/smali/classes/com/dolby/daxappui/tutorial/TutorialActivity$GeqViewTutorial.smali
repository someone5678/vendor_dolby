.class public Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;
.super Landroid/view/View;
.source "TutorialActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dolby/daxappui/tutorial/TutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GeqViewTutorial"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGeqData:[F

.field private mHLinePaint:Landroid/graphics/Paint;

.field private mRecPaint:Landroid/graphics/Paint;

.field private mRect:Landroid/graphics/Rect;

.field private mStokeLinePath:Landroid/graphics/Path;

.field private mStrokeLinePaint:Landroid/graphics/Paint;

.field private mTitlePaint:Landroid/graphics/Paint;

.field private final yTitlesStrings:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 457
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const-string v0, "+12dB"

    const-string v1, "0dB"

    const-string v2, "-12dB"

    .line 437
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->yTitlesStrings:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [F

    .line 438
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mGeqData:[F

    .line 458
    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    .line 459
    invoke-direct {p0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->init()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x40a00000    # 5.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 463
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p2, "+12dB"

    const-string v0, "0dB"

    const-string v1, "-12dB"

    .line 437
    filled-new-array {p2, v0, v1}, [Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->yTitlesStrings:[Ljava/lang/String;

    const/16 p2, 0xa

    new-array p2, p2, [F

    .line 438
    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mGeqData:[F

    .line 464
    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    .line 465
    invoke-direct {p0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->init()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x40a00000    # 5.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private init()V
    .locals 3

    .line 443
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    .line 444
    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 445
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    .line 446
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    .line 447
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    .line 448
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    .line 449
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x4

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 450
    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 451
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    .line 453
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    return-void

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    .line 470
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 471
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 472
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v8, v2, Landroid/util/DisplayMetrics;->density:F

    if-eqz v1, :cond_0

    .line 475
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060089    # @dimen/geq_bar_end_padding '24.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0

    .line 477
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 479
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v9, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    sub-int v10, v3, v4

    int-to-float v3, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v3, v11

    .line 481
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v12

    const/4 v14, 0x1

    if-ne v12, v14, :cond_1

    .line 485
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_1

    .line 487
    :cond_1
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 491
    :goto_1
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f06008d    # @dimen/geq_text_size '11.0sp'

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 492
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050034    # @color/colorGeqGainText '#94000000'

    iget-object v15, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v15

    invoke-virtual {v5, v6, v15}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 493
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    .line 494
    iget v5, v4, Landroid/graphics/Paint$FontMetrics;->descent:F

    float-to-int v5, v5

    .line 495
    iget v6, v4, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v6, v4

    .line 496
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    iget-object v15, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->yTitlesStrings:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v4, v15}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    if-ne v12, v14, :cond_2

    .line 499
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v15

    int-to-float v15, v15

    sub-float v4, v15, v4

    :cond_2
    move/from16 v15, v16

    .line 501
    :goto_2
    iget-object v13, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->yTitlesStrings:[Ljava/lang/String;

    array-length v13, v13

    if-ge v15, v13, :cond_5

    if-nez v15, :cond_3

    int-to-float v13, v5

    sub-float v13, v6, v13

    .line 505
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    :goto_3
    int-to-float v11, v11

    add-float/2addr v13, v11

    goto :goto_4

    :cond_3
    if-ne v15, v14, :cond_4

    const/high16 v11, 0x40000000    # 2.0f

    div-float v13, v6, v11

    add-float/2addr v13, v3

    int-to-float v14, v5

    sub-float/2addr v13, v14

    .line 507
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    int-to-float v14, v14

    add-float/2addr v13, v14

    goto :goto_4

    :cond_4
    const/high16 v11, 0x40000000    # 2.0f

    mul-float v13, v3, v11

    int-to-float v11, v5

    sub-float/2addr v13, v11

    .line 509
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    goto :goto_3

    .line 511
    :goto_4
    iget-object v11, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->yTitlesStrings:[Ljava/lang/String;

    aget-object v11, v11, v15

    iget-object v14, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mTitlePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v11, v4, v13, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v15, v15, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v14, 0x1

    goto :goto_2

    .line 514
    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06008c    # @dimen/geq_text_and_bar_margin '50.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 515
    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mGeqData:[F

    array-length v3, v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v11, v2

    if-eqz v1, :cond_6

    int-to-float v1, v11

    const v2, 0x3f59999a    # 0.85f

    mul-float/2addr v2, v1

    const v3, 0x3e19999a    # 0.15f

    goto :goto_5

    :cond_6
    int-to-float v1, v11

    const v2, 0x3f6b851f    # 0.92f

    mul-float/2addr v2, v1

    const v3, 0x3da3d70a    # 0.08f

    :goto_5
    mul-float/2addr v1, v3

    move v14, v1

    move v13, v2

    .line 525
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v12, v2, :cond_7

    .line 527
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    :cond_7
    move v15, v1

    move/from16 v1, v16

    .line 530
    :goto_6
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mGeqData:[F

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 531
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050030    # @color/colorGeqBackgroundBar '#12000000'

    iget-object v5, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 532
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    const/4 v2, 0x1

    if-ne v12, v2, :cond_9

    .line 534
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v11

    sub-int v4, v15, v3

    iput v4, v2, Landroid/graphics/Rect;->left:I

    if-nez v1, :cond_8

    int-to-float v3, v15

    sub-float/2addr v3, v14

    float-to-int v3, v3

    .line 536
    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_8

    :cond_8
    int-to-float v3, v3

    sub-float/2addr v3, v13

    float-to-int v3, v3

    sub-int v3, v15, v3

    .line 538
    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_8

    :cond_9
    if-nez v1, :cond_a

    .line 542
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v15

    add-float/2addr v3, v14

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_7

    .line 544
    :cond_a
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v11

    int-to-float v3, v3

    sub-float/2addr v3, v13

    float-to-int v3, v3

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 546
    :goto_7
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v11

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 548
    :goto_8
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 549
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v3, v10

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    const/4 v2, 0x3

    if-eq v2, v1, :cond_b

    .line 552
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_a

    .line 554
    :cond_b
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05004e    # @color/colorSelectedGeqBackgroundBar '@android:color/system_accent1_100'

    iget-object v5, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 555
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    const/4 v2, 0x1

    if-ne v12, v2, :cond_c

    .line 557
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    mul-int/lit8 v3, v11, 0x3

    sub-int v3, v15, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v3, v11, 0x4

    int-to-float v3, v3

    add-float/2addr v3, v14

    float-to-int v3, v3

    sub-int v3, v15, v3

    .line 558
    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_9

    .line 560
    :cond_c
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    mul-int/lit8 v3, v11, 0x3

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v3, v11, 0x4

    int-to-float v3, v3

    add-float/2addr v3, v14

    float-to-int v3, v3

    add-int/2addr v3, v15

    .line 561
    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 564
    :goto_9
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v3, v10, 0x2

    add-int/2addr v2, v3

    int-to-float v3, v3

    const v4, 0x3ed55555

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 567
    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 568
    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 569
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_6

    :cond_d
    move/from16 v6, v16

    .line 573
    :goto_b
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mGeqData:[F

    array-length v2, v1

    if-ge v6, v2, :cond_14

    .line 574
    aget v1, v1, v6

    .line 576
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v2}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 580
    const/4 v2, 0x1

    if-ne v12, v2, :cond_f

    .line 581
    if-nez v6, :cond_e

    .line 582
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v15

    sub-float/2addr v3, v14

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_c

    .line 584
    :cond_e
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v11

    int-to-float v3, v3

    sub-float/2addr v3, v13

    float-to-int v3, v3

    sub-int v3, v15, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 586
    :goto_c
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v11

    sub-int v3, v15, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_e

    .line 588
    :cond_f
    if-nez v6, :cond_10

    .line 589
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v15

    add-float/2addr v3, v14

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto :goto_d

    .line 591
    :cond_10
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v11

    int-to-float v3, v3

    sub-float/2addr v3, v13

    float-to-int v3, v3

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 593
    :goto_d
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    add-int/lit8 v3, v6, 0x1

    mul-int/2addr v3, v11

    add-int/2addr v3, v15

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 595
    :goto_e
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050031    # @color/colorGeqBar '@android:color/system_accent1_600'

    iget-object v5, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 607
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v3, v10, 0x2

    add-int/2addr v2, v3

    .line 608
    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    int-to-float v3, v3

    const/high16 v5, 0x41400000    # 12.0f

    div-float/2addr v1, v5

    mul-float/2addr v3, v1

    float-to-int v1, v3

    sub-int/2addr v2, v1

    iput v2, v4, Landroid/graphics/Rect;->top:I

    .line 609
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, v10

    iput v1, v4, Landroid/graphics/Rect;->bottom:I

    .line 611
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRecPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const v1, 0x7f06008f    # @dimen/geq_top_line_margin '2.0dp'

    const/4 v4, 0x3

    if-ne v4, v6, :cond_13

    .line 614
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050050    # @color/colorSelectedGeqBarTopLine '#ff85b1c6'

    iget-object v5, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 615
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v4, v8, v3

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 616
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    .line 618
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    sub-float v5, v3, v1

    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v3, v1

    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    move-object/from16 v20, v1

    move-object/from16 v1, p1

    move/from16 v21, v3

    move v3, v4

    const/16 v17, 0x3

    move v4, v5

    const v9, 0x7f050032    # @color/colorGeqBarTopLine '#8cf8fafb'

    move/from16 v5, v21

    move/from16 v16, v6

    move-object/from16 v6, v20

    .line 616
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 622
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 623
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 624
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 625
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 626
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    const/4 v6, 0x1

    if-ne v12, v6, :cond_11

    .line 628
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    sub-int v2, v15, v11

    int-to-float v2, v2

    add-float/2addr v2, v13

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 629
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_f

    .line 631
    :cond_11
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    add-int v2, v15, v11

    int-to-float v2, v2

    sub-float/2addr v2, v13

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 632
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 634
    :goto_f
    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStokeLinePath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mStrokeLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 636
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008a    # @drawable/image_on_top_geqbar 'res/drawable/image_on_top_geqbar.xml'

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 638
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06008e    # @dimen/geq_top_image_size '6.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v3, v11, v3

    float-to-int v5, v14

    add-int/2addr v3, v5

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 639
    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v9, v11

    add-int/2addr v9, v5

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v3, v9

    .line 640
    iget-object v5, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v5, v9

    .line 641
    iget-object v9, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v9, v4

    .line 642
    invoke-virtual {v1, v2, v5, v3, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 643
    invoke-virtual {v1, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_12
    const/high16 v9, 0x40000000    # 2.0f

    const/16 v19, 0x1

    goto :goto_10

    :cond_13
    move/from16 v17, v4

    move/from16 v16, v6

    const v9, 0x7f050032    # @color/colorGeqBarTopLine '#8cf8fafb'

    .line 646
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 647
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    const/high16 v9, 0x40000000    # 2.0f

    mul-float v3, v8, v9

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 648
    iget-object v2, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    .line 650
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    sub-float v5, v3, v1

    iget-object v1, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v6, v1

    iget-object v3, v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;->mHLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move-object/from16 v18, v3

    move v3, v4

    move v4, v5

    move v5, v6

    const/16 v19, 0x1

    move-object/from16 v6, v18

    .line 648
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_10
    add-int/lit8 v6, v16, 0x1

    const v9, 0x7f06008a    # @dimen/geq_border_vertical_margin '14.0dp'

    goto/16 :goto_b

    :cond_14
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 659
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method
