.class Lcom/dolby/daxappui/tutorial/TutorialActivity$4;
.super Ljava/lang/Object;
.source "TutorialActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dolby/daxappui/tutorial/TutorialActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;


# direct methods
.method constructor <init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;)V
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 3

    .line 385
    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-static {v0}, Lcom/dolby/daxappui/tutorial/TutorialActivity;->access$400(Lcom/dolby/daxappui/tutorial/TutorialActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-static {v1}, Lcom/dolby/daxappui/tutorial/TutorialActivity;->access$300(Lcom/dolby/daxappui/tutorial/TutorialActivity;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    const v2, 0x7f0700d1    # @drawable/tutorial_dot_unselected 'res/drawable/tutorial_dot_unselected.xml'

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 386
    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-static {v0}, Lcom/dolby/daxappui/tutorial/TutorialActivity;->access$400(Lcom/dolby/daxappui/tutorial/TutorialActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    const v2, 0x7f0700d0    # @drawable/tutorial_dot_selected 'res/drawable/tutorial_dot_selected.xml'

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 387
    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-static {v0, p1}, Lcom/dolby/daxappui/tutorial/TutorialActivity;->access$302(Lcom/dolby/daxappui/tutorial/TutorialActivity;I)I

    .line 388
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    invoke-static {p0, p1}, Lcom/dolby/daxappui/tutorial/TutorialActivity;->access$502(Lcom/dolby/daxappui/tutorial/TutorialActivity;I)I

    return-void
.end method
