.class public Lcom/dolby/daxappui/tutorial/TutorialActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "TutorialActivity.java"

# interfaces
.implements Lcom/dolby/daxappui/IDsFragObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;,
        Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;
    }
.end annotation


# instance fields
.field private Mview:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private dots:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private item_view01:Landroid/view/View;

.field private item_view02:Landroid/view/View;

.field private mCurrentSelectedPage:I

.field mTutorialViewPager:Landroid/support/v4/view/ViewPager;

.field private oldPosition:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 59
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    .line 62
    iput v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->oldPosition:I

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->Mview:Ljava/util/List;

    .line 66
    iput v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->mCurrentSelectedPage:I

    return-void
.end method

.method static synthetic access$000(Lcom/dolby/daxappui/tutorial/TutorialActivity;)Landroid/view/View;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view01:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$100(Lcom/dolby/daxappui/tutorial/TutorialActivity;I)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/dolby/daxappui/tutorial/TutorialActivity;->updateLayout(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/dolby/daxappui/tutorial/TutorialActivity;)Landroid/view/View;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view02:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$300(Lcom/dolby/daxappui/tutorial/TutorialActivity;)I
    .locals 0

    .line 59
    iget p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->oldPosition:I

    return p0
.end method

.method static synthetic access$302(Lcom/dolby/daxappui/tutorial/TutorialActivity;I)I
    .locals 0

    .line 59
    iput p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->oldPosition:I

    return p1
.end method

.method static synthetic access$400(Lcom/dolby/daxappui/tutorial/TutorialActivity;)Ljava/util/List;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->dots:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$502(Lcom/dolby/daxappui/tutorial/TutorialActivity;I)I
    .locals 0

    .line 59
    iput p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->mCurrentSelectedPage:I

    return p1
.end method

.method private updateLayout(I)V
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    const/4 v2, 0x0

    .line 69
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 70
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v5, 0x2

    new-array v6, v5, [I

    .line 72
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0601d2    # @dimen/tutorial_text_and_tooltip_spacing '12.0dp'

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 73
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0601d3    # @dimen/tutorial_tooltip_and_tooltip_spacing '14.0dp'

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 74
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v8

    .line 75
    new-instance v9, Landroid/util/DisplayMetrics;

    invoke-direct {v9}, Landroid/util/DisplayMetrics;-><init>()V

    .line 76
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 77
    iget v10, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 78
    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 79
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v11

    const/16 v14, 0x15

    const/4 v15, 0x1

    if-eq v1, v15, :cond_6

    if-eq v1, v5, :cond_0

    goto/16 :goto_b

    :cond_0
    const v1, 0x7f090067    # @id/deText

    .line 198
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 199
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    const v10, 0x7f090068    # @id/deText_tutorial

    .line 201
    invoke-virtual {v0, v10}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 202
    invoke-virtual {v10, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 204
    invoke-virtual {v10}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    move-object/from16 v12, v16

    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    if-ne v8, v15, :cond_1

    .line 206
    invoke-virtual {v12, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 207
    aget v13, v6, v2

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_0

    .line 209
    :cond_1
    aget v13, v6, v2

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 211
    :goto_0
    aget v13, v6, v15

    iput v13, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 212
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v10, 0x7f090066    # @id/deButton_tutorial

    const v12, 0x7f090064    # @id/deButton

    if-eqz v11, :cond_3

    .line 216
    invoke-virtual {v0, v12}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/SeekBar;

    .line 217
    invoke-virtual {v11, v6}, Landroid/widget/SeekBar;->getLocationOnScreen([I)V

    const v12, 0x7f090065    # @id/deButtonView_tutorial

    .line 219
    invoke-virtual {v0, v12}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout;

    .line 220
    invoke-virtual {v12, v3, v4}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 221
    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    if-ne v8, v15, :cond_2

    .line 223
    invoke-virtual {v3, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 224
    aget v4, v6, v2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_1

    .line 226
    :cond_2
    aget v4, v6, v2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 228
    :goto_1
    aget v4, v6, v15

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 229
    invoke-virtual {v12, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    invoke-virtual {v0, v10}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    .line 231
    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 232
    invoke-virtual {v11}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_3

    .line 234
    :cond_3
    invoke-virtual {v0, v12}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/SeekBar;

    .line 235
    invoke-virtual {v11, v6}, Landroid/widget/SeekBar;->getLocationOnScreen([I)V

    .line 237
    invoke-virtual {v0, v10}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/SeekBar;

    .line 238
    invoke-virtual {v10, v3, v4}, Landroid/widget/SeekBar;->measure(II)V

    .line 239
    invoke-virtual {v10}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 240
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dolby/daxappui/DAXApplication;->getIeqViewWidth()F

    move-result v4

    float-to-int v4, v4

    mul-int/2addr v4, v5

    .line 241
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f060097    # @dimen/ieq_horizontal_spacing_normal '2.0dp'

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v12

    mul-int/2addr v12, v5

    add-int/2addr v4, v12

    .line 242
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0601c5    # @dimen/profile_switch_track_width '46.0dp'

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v12

    add-int/2addr v4, v12

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-ne v8, v15, :cond_4

    .line 244
    invoke-virtual {v3, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 245
    aget v4, v6, v2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_2

    .line 247
    :cond_4
    aget v4, v6, v2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 249
    :goto_2
    aget v4, v6, v15

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 250
    invoke-virtual {v10, v3}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    invoke-virtual {v10, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 252
    invoke-virtual {v11}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v10, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    :goto_3
    const v2, 0x7f09006a    # @id/de_tooltip

    .line 256
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 257
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/16 v4, 0xc

    .line 258
    invoke-virtual {v2, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    new-array v3, v5, [I

    .line 261
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 262
    aget v1, v3, v15

    aget v4, v6, v15

    if-ge v1, v4, :cond_5

    .line 263
    aget v1, v3, v15

    goto :goto_4

    .line 265
    :cond_5
    aget v1, v6, v15

    :goto_4
    sub-int/2addr v9, v1

    add-int/2addr v9, v7

    .line 267
    iput v9, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 268
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_b

    :cond_6
    const v1, 0x7f0900d7    # @id/ieqText

    .line 84
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 85
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    const v1, 0x7f0900d8    # @id/ieqText_tutorial

    .line 87
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 88
    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 89
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    if-ne v8, v15, :cond_7

    .line 91
    invoke-virtual {v5, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 92
    aget v11, v6, v2

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_5

    .line 94
    :cond_7
    aget v11, v6, v2

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 96
    :goto_5
    aget v11, v6, v15

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 97
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0900d5    # @id/ieqName

    .line 100
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 101
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    const v1, 0x7f0900d6    # @id/ieqName_tutorial

    .line 103
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 104
    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 105
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    if-ne v8, v15, :cond_8

    .line 107
    invoke-virtual {v5, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 108
    aget v11, v6, v2

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_6

    .line 110
    :cond_8
    aget v11, v6, v2

    invoke-virtual {v5, v11}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 112
    :goto_6
    aget v11, v6, v15

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 113
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f090090    # @id/equalizerListView

    .line 116
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 117
    invoke-virtual {v1, v6}, Landroid/widget/GridView;->getLocationOnScreen([I)V

    const v5, 0x7f090092    # @id/equalizerListView_tutorial

    .line 119
    invoke-virtual {v0, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/GridView;

    .line 120
    invoke-virtual {v5, v3, v4}, Landroid/widget/GridView;->measure(II)V

    .line 121
    invoke-virtual {v5}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 122
    invoke-virtual {v1}, Landroid/widget/GridView;->getWidth()I

    move-result v12

    iput v12, v11, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-ne v8, v15, :cond_9

    .line 124
    aget v12, v6, v2

    sub-int v12, v10, v12

    invoke-virtual {v1}, Landroid/widget/GridView;->getWidth()I

    move-result v1

    sub-int/2addr v12, v1

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_7

    .line 126
    :cond_9
    aget v1, v6, v2

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 128
    :goto_7
    aget v1, v6, v15

    iput v1, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 129
    invoke-virtual {v5, v11}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    new-instance v1, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;

    invoke-direct {v1, v0, v0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;-><init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;Landroid/content/Context;)V

    .line 131
    invoke-virtual {v5, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 132
    invoke-virtual {v5, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    const v1, 0x7f09008c    # @id/equalizerListOff

    .line 134
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 135
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    const v11, 0x7f09008f    # @id/equalizerListOff_tutorial

    .line 137
    invoke-virtual {v0, v11}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 138
    invoke-virtual {v11, v3, v4}, Landroid/widget/LinearLayout;->measure(II)V

    .line 139
    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 140
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    iput v1, v12, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-ne v8, v15, :cond_a

    .line 142
    invoke-virtual {v12, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 143
    aget v1, v6, v2

    invoke-virtual {v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_8

    .line 145
    :cond_a
    aget v1, v6, v2

    invoke-virtual {v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 147
    :goto_8
    aget v1, v6, v15

    iput v1, v12, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 148
    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0900d9    # @id/ieq_tooltip

    .line 151
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 152
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v12, 0x3

    .line 153
    invoke-virtual {v5}, Landroid/widget/GridView;->getId()I

    move-result v5

    invoke-virtual {v11, v12, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 154
    iput v7, v11, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 155
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0900a9    # @id/geqText

    .line 158
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 159
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    const v1, 0x7f0900ab    # @id/geqText_tutorial

    .line 161
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 163
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    if-ne v8, v15, :cond_b

    .line 165
    invoke-virtual {v3, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 166
    aget v4, v6, v2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_9

    .line 168
    :cond_b
    aget v4, v6, v2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 170
    :goto_9
    aget v4, v6, v15

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 171
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0900ae    # @id/geq_tooltip

    .line 173
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 174
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/16 v5, 0xc

    .line 175
    invoke-virtual {v3, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 176
    aget v4, v6, v15

    sub-int/2addr v9, v4

    add-int/2addr v9, v7

    iput v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 177
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0900ac    # @id/geqView

    .line 180
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/dolby/daxappui/GeqView;

    .line 181
    invoke-virtual {v1, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    const v3, 0x7f0900ad    # @id/geqView_tutorial

    .line 183
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$GeqViewTutorial;

    const/high16 v3, 0x40000000    # 2.0f

    .line 184
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 185
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 186
    invoke-virtual {v0, v4, v3}, Landroid/view/View;->measure(II)V

    .line 187
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    if-ne v8, v15, :cond_c

    .line 189
    aget v2, v6, v2

    sub-int/2addr v10, v2

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v10, v1

    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_a

    .line 191
    :cond_c
    aget v1, v6, v2

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 193
    :goto_a
    aget v1, v6, v15

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 194
    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_b
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .line 277
    invoke-static {p1}, Lcom/dolby/daxappui/DAXApplication$ConfigurationWrapper;->wrapLocale(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/app/Activity;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method public chooseProfile(I)V
    .locals 0

    return-void
.end method

.method public getActivePort()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;
    .locals 0

    const/4 p0, 0x0

    return-object p0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 282
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 284
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    .line 286
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 288
    :cond_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 290
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/dolby/daxappui/DAXApplication;

    invoke-virtual {v2}, Lcom/dolby/daxappui/DAXApplication;->getProductVersion()Ljava/lang/String;

    move-result-object v2

    .line 291
    const v3, 0x7f0c0089    # @layout/tutorial_activity_main 'res/layout/tutorial_activity_main.xml'

    .line 292
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatActivity;->setContentView(I)V

    const v3, 0x7f0901a4    # @id/toolbar

    .line 294
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/Toolbar;

    .line 295
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 297
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 298
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 299
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 300
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 301
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 302
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    const v4, 0x7f070087    # @drawable/ic_sidemenu_titlebar 'res/drawable/ic_sidemenu_titlebar.xml'

    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    :cond_1
    const/16 v3, 0x8

    const/4 v4, 0x3

    if-eqz p1, :cond_6

    .line 306
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 307
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object p1

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const-string v6, "#161819"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v5}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    const p1, 0x7f090148    # @id/presetsListView

    .line 309
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    if-eqz p1, :cond_3

    .line 311
    invoke-virtual {p1, v4, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_3
    const p1, 0x7f09014d    # @id/profileName

    .line 313
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_4

    .line 315
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f100031    # @string/custom 'Custom'

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const p1, 0x7f0900d5    # @id/ieqName

    .line 317
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_5

    .line 319
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f10007f    # @string/no_effect 'No effect'

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    const p1, 0x7f090149    # @id/profileDescription

    .line 322
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-eqz p1, :cond_7

    .line 324
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_6
    const p1, 0x7f090151    # @id/profileViewpager

    .line 327
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/dolby/daxappui/CustomViewPager;

    if-eqz p1, :cond_7

    .line 329
    invoke-virtual {p1, v4, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    :cond_7
    :goto_1
    const p1, 0x7f0901b1    # @id/tutorialMainLayout

    .line 333
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 334
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v5, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;

    invoke-direct {v5, p0, p1, v2}, Lcom/dolby/daxappui/tutorial/TutorialActivity$1;-><init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;Landroid/widget/LinearLayout;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const p1, 0x7f0901b2    # @id/tutorialViewPager

    .line 347
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/support/v4/view/ViewPager;

    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->mTutorialViewPager:Landroid/support/v4/view/ViewPager;

    .line 348
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->dots:Ljava/util/List;

    .line 349
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->dots:Ljava/util/List;

    const v1, 0x7f090081    # @id/dot_1

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v5, 0x7f0c008a    # @layout/tutorial_page_1 'res/layout/tutorial_page_1.xml'

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view01:Landroid/view/View;

    .line 351
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->Mview:Ljava/util/List;

    iget-object v5, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view01:Landroid/view/View;

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view01:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance v5, Lcom/dolby/daxappui/tutorial/TutorialActivity$2;

    invoke-direct {v5, p0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$2;-><init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;)V

    invoke-virtual {p1, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 360
    invoke-virtual {v2, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string v0, "DS1"

    .line 361
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const v0, 0x7f090082    # @id/dot_2

    if-nez p1, :cond_8

    .line 362
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->dots:Ljava/util/List;

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0c008b    # @layout/tutorial_page_2 'res/layout/tutorial_page_2.xml'

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view02:Landroid/view/View;

    .line 364
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->Mview:Ljava/util/List;

    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view02:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->item_view02:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$3;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$3;-><init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_2

    .line 373
    :cond_8
    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 374
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 375
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 376
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 379
    :goto_2
    iget-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->mTutorialViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/tutorial/TutorialActivity$4;-><init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;)V

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 396
    new-instance p1, Lcom/dolby/daxappui/tutorial/ViewPagerAdapter;

    iget-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->Mview:Ljava/util/List;

    invoke-direct {p1, v0}, Lcom/dolby/daxappui/tutorial/ViewPagerAdapter;-><init>(Ljava/util/List;)V

    .line 397
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->mTutorialViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 426
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 427
    iget p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity;->mCurrentSelectedPage:I

    const-string v0, "currentSelectedPage"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public resetProfile(I)V
    .locals 0

    return-void
.end method
