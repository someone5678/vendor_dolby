.class public Lcom/dolby/daxappui/FragProfilePanel;
.super Landroid/support/v4/app/Fragment;
.source "FragProfilePanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

.field private mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

.field private mIeqName:[Ljava/lang/String;

.field private mMask:Landroid/widget/FrameLayout;

.field public mNum:I

.field private mProductVersion:Ljava/lang/String;

.field private mProfileNames:[Ljava/lang/String;

.field private profileImgId_dax2:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, -0x1

    .line 36
    iput v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mNum:I

    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    const/4 v0, 0x4

    new-array v0, v0, [I

    .line 42
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->profileImgId_dax2:[I

    return-void

    :array_0
    .array-data 4
        0x7f07006f    # @drawable/ic_dynamic_profile_panel 'res/drawable/ic_dynamic_profile_panel.xml'
        0x7f070076    # @drawable/ic_movie_profile_panel 'res/drawable/ic_movie_profile_panel.xml'
        0x7f07007b    # @drawable/ic_music_profile_panel 'res/drawable/ic_music_profile_panel.xml'
        0x7f07006d    # @drawable/ic_custom_profile_panel 'res/drawable/ic_custom_profile_panel.xml'
    .end array-data
.end method

.method public static newInstance(I)Lcom/dolby/daxappui/FragProfilePanel;
    .locals 3

    .line 53
    new-instance v0, Lcom/dolby/daxappui/FragProfilePanel;

    invoke-direct {v0}, Lcom/dolby/daxappui/FragProfilePanel;-><init>()V

    .line 54
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "num"

    .line 55
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 56
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 6

    .line 128
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 130
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/dolby/daxappui/IDsFragObserver;

    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dolby/daxappui/DAXApplication;->getProfileNames()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mProfileNames:[Ljava/lang/String;

    .line 135
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dolby/daxappui/DAXApplication;->getProductVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mProductVersion:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    .line 136
    iput-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    .line 137
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    const v1, 0x7f10002e    # @string/bright 'Bright'

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 138
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    const v1, 0x7f10002c    # @string/balanced 'Balanced'

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 139
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    const/4 v1, 0x2

    const v4, 0x7f100090    # @string/warm 'Warm'

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 140
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    const v1, 0x7f10007f    # @string/no_effect 'No effect'

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x3

    aput-object v1, v0, v4

    .line 142
    iput-object p1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    .line 145
    iget-object p1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const-string v0, "dax_dea_default"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 146
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "firstrun"

    .line 147
    invoke-interface {p1, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 148
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 149
    iget-object p1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 150
    iget-object p1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {p1}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p1

    sget-object v1, Lcom/dolby/dax/DsParams;->DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

    invoke-virtual {v1}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result v1

    invoke-virtual {p1, v3, v1}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(II)[I

    move-result-object p1

    .line 151
    aget p1, p1, v2

    const-string v1, "DialogEnhancerAmountForMovie"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 152
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {p0}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object p0

    sget-object p1, Lcom/dolby/dax/DsParams;->DialogEnhancementAmount:Lcom/dolby/dax/DsParams;

    invoke-virtual {p1}, Lcom/dolby/dax/DsParams;->toInt()I

    move-result p1

    invoke-virtual {p0, v4, p1}, Lcom/dolby/dax/DolbyAudioEffect;->getDapParameter(II)[I

    move-result-object p0

    .line 153
    aget p0, p0, v2

    const-string p1, "DialogEnhancerAmountForCustom"

    invoke-interface {v0, p1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 154
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_0
    const-string p0, "FragProfilePanel"

    const-string p1, "mAudioEffect is null when save default dea at firstrun"

    .line 156
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    .line 132
    :catch_0
    new-instance p0, Ljava/lang/ClassCastException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement IDsFragObserver"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 379
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/dolby/daxappui/IDsFragObserver;->getDolbyAudioEffect()Lcom/dolby/dax/DolbyAudioEffect;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 381
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v1, 0x7f09014e    # @id/profileResetButton

    if-ne v1, p1, :cond_1

    .line 383
    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->getProfile()I

    move-result p1

    .line 384
    sget-object v0, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v0, v1, p1}, Lcom/dolby/daxappui/DsClientSettings;->resetProfileSpecificSettings(Lcom/dolby/daxappui/IDsFragObserver;I)V

    .line 385
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {p0, p1}, Lcom/dolby/daxappui/IDsFragObserver;->resetProfile(I)V

    goto :goto_1

    :cond_1
    const v0, 0x7f09008c    # @id/equalizerListOff

    if-ne v0, p1, :cond_3

    .line 387
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    const v0, 0x7f0900d2    # @id/icon_off

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 388
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09008d    # @id/equalizerListOffLayout

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 390
    iget-object v1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v2, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f07007e    # @drawable/ic_none_on_ieq 'res/drawable/ic_none_on_ieq.xml'

    .line 391
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 393
    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    const/4 p1, 0x3

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/dolby/daxappui/EqualizerAdapter;->setIeqSelection(IZ)V

    goto :goto_1

    :cond_2
    const-string p0, "FragProfilePanel"

    const-string p1, "onClick(): Dolby audio effect is null!"

    .line 396
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 122
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "num"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    iput p1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mNum:I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16

    move-object/from16 v0, p0

    const v1, 0x7f0c0082    # @layout/profile 'res/layout/profile.xml'

    const/4 v2, 0x0

    move-object/from16 v3, p1

    .line 164
    invoke-virtual {v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 165
    iget-object v2, v0, Lcom/dolby/daxappui/FragProfilePanel;->mProductVersion:Ljava/lang/String;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 167
    new-instance v5, Landroid/widget/FrameLayout;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    .line 168
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v5, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 171
    iget-object v7, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v5}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v8

    const v9, 0x7f05003f    # @color/colorMaskBackground '#00000000'

    invoke-virtual {v7, v9, v8}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 173
    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 175
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    .line 176
    iget v8, v0, Lcom/dolby/daxappui/FragProfilePanel;->mNum:I

    const v9, 0x7f09008d    # @id/equalizerListOffLayout

    const v10, 0x7f0900d2    # @id/icon_off

    const-string v11, "DAX3"

    const v12, 0x7f090069    # @id/deView

    const v13, 0x7f09008c    # @id/equalizerListOff

    const v14, 0x7f0900d5    # @id/ieqName

    const v15, 0x7f090090    # @id/equalizerListView

    const/16 v3, 0x8

    const/4 v6, 0x3

    if-eqz v5, :cond_8

    .line 179
    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/GridView;

    if-eqz v5, :cond_1

    .line 181
    invoke-virtual {v1, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 182
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 183
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 185
    sget-object v15, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v15, v4}, Lcom/dolby/daxappui/DsClientSettings;->getIeqPreset(Lcom/dolby/daxappui/IDsFragObserver;)I

    move-result v4

    .line 186
    invoke-virtual {v1, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 187
    iget-object v15, v0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    aget-object v15, v15, v4

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-ne v4, v6, :cond_0

    .line 190
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v14, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v4, v14}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v4, 0x7f07007e    # @drawable/ic_none_on_ieq 'res/drawable/ic_none_on_ieq.xml'

    .line 191
    invoke-virtual {v10, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 193
    :cond_0
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v14, 0x7f070061    # @drawable/background_with_shadow 'res/drawable/background_with_shadow.xml'

    invoke-static {v4, v14}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v4, 0x7f07007d    # @drawable/ic_none_off_ieq 'res/drawable/ic_none_off_ieq.xml'

    .line 194
    invoke-virtual {v10, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 196
    :goto_0
    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    new-instance v4, Lcom/dolby/daxappui/EqualizerAdapter;

    iget-object v9, v0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    invoke-direct {v4, v9, v0}, Lcom/dolby/daxappui/EqualizerAdapter;-><init>(Landroid/content/Context;Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;)V

    iput-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    .line 199
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    invoke-virtual {v5, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 203
    :cond_1
    invoke-virtual {v1, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 204
    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-eq v8, v7, :cond_3

    if-ne v8, v6, :cond_2

    goto :goto_1

    .line 216
    :cond_2
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3

    :cond_3
    :goto_1
    const v2, 0x7f090064    # @id/deButton

    .line 206
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    .line 207
    sget-object v3, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v3, v5}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 209
    sget-object v3, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v3, v5}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    .line 211
    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 213
    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 214
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3

    .line 219
    :cond_5
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :goto_3
    const v2, 0x7f0900ac    # @id/geqView

    .line 227
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/dolby/daxappui/GeqView;

    if-eqz v2, :cond_6

    .line 228
    iget v3, v2, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_6

    .line 229
    invoke-virtual {v2}, Lcom/dolby/daxappui/GeqView;->onUpdateGeqData()V

    :cond_6
    const v2, 0x7f09014e    # @id/profileResetButton

    .line 233
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v2, :cond_1a

    .line 235
    sget-object v3, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v3, v4, v8}, Lcom/dolby/daxappui/DsClientSettings;->isProfileSpecificSettingsModified(Lcom/dolby/daxappui/IDsFragObserver;I)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    .line 237
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_7
    const/4 v3, 0x4

    .line 239
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 241
    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_10

    :cond_8
    const v4, 0x7f09014b    # @id/profileIcon

    .line 245
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 246
    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->profileImgId_dax2:[I

    iget v7, v0, Lcom/dolby/daxappui/FragProfilePanel;->mNum:I

    aget v5, v5, v7

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    const v4, 0x7f09014d    # @id/profileName

    .line 247
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 248
    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mProfileNames:[Ljava/lang/String;

    aget-object v5, v5, v8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0900d4    # @id/ieqLayout

    .line 251
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    const v5, 0x7f060097    # @dimen/ieq_horizontal_spacing_normal '2.0dp'

    const/4 v7, 0x2

    if-eq v8, v7, :cond_a

    if-ne v8, v6, :cond_9

    goto :goto_5

    .line 291
    :cond_9
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move v3, v12

    goto/16 :goto_9

    :cond_a
    :goto_5
    const/4 v3, 0x0

    .line 253
    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 254
    sget-object v3, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v3, v4}, Lcom/dolby/daxappui/DsClientSettings;->getIeqPreset(Lcom/dolby/daxappui/IDsFragObserver;)I

    move-result v3

    .line 255
    invoke-virtual {v1, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 256
    iget-object v14, v0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    aget-object v14, v14, v3

    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/GridView;

    .line 259
    invoke-virtual {v1, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 260
    invoke-virtual {v4}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    .line 261
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v15

    invoke-virtual {v15}, Lcom/dolby/daxappui/DAXApplication;->getIeqViewWidth()F

    move-result v15

    float-to-int v15, v15

    mul-int/2addr v15, v6

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v12

    mul-int/2addr v12, v7

    add-int/2addr v15, v12

    if-nez v14, :cond_b

    .line 263
    new-instance v14, Landroid/view/ViewGroup$LayoutParams;

    const/4 v12, -0x2

    invoke-direct {v14, v15, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    goto :goto_6

    .line 265
    :cond_b
    iput v15, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 267
    :goto_6
    invoke-virtual {v4, v14}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 269
    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    .line 270
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v14

    invoke-virtual {v14}, Lcom/dolby/daxappui/DAXApplication;->getIeqViewWidth()F

    move-result v14

    float-to-int v14, v14

    if-nez v12, :cond_c

    .line 272
    new-instance v12, Landroid/view/ViewGroup$LayoutParams;

    const/4 v15, -0x2

    invoke-direct {v12, v14, v15}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    goto :goto_7

    .line 274
    :cond_c
    iput v14, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 276
    :goto_7
    invoke-virtual {v13, v12}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 277
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 278
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    if-ne v3, v6, :cond_d

    .line 280
    iget-object v12, v0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v14, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v12, v14}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v9, 0x7f07007e    # @drawable/ic_none_on_ieq 'res/drawable/ic_none_on_ieq.xml'

    .line 281
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8

    .line 283
    :cond_d
    iget-object v12, v0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v14, 0x7f070061    # @drawable/background_with_shadow 'res/drawable/background_with_shadow.xml'

    invoke-static {v12, v14}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v9, 0x7f07007d    # @drawable/ic_none_off_ieq 'res/drawable/ic_none_off_ieq.xml'

    .line 284
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 286
    :goto_8
    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    new-instance v9, Lcom/dolby/daxappui/EqualizerAdapter;

    iget-object v10, v0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10, v0}, Lcom/dolby/daxappui/EqualizerAdapter;-><init>(Landroid/content/Context;Lcom/dolby/daxappui/EqualizerAdapter$IPresetListener;)V

    iput-object v9, v0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    .line 288
    iget-object v9, v0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    invoke-virtual {v4, v9}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 289
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    const/4 v9, 0x0

    invoke-virtual {v4, v3, v9}, Lcom/dolby/daxappui/EqualizerAdapter;->setIeqSelection(IZ)V

    const v3, 0x7f090069    # @id/deView

    .line 295
    :goto_9
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 296
    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    if-eq v8, v2, :cond_f

    if-ne v8, v6, :cond_e

    goto :goto_a

    :cond_e
    const/16 v2, 0x8

    .line 318
    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move v4, v2

    const/4 v2, 0x0

    goto :goto_c

    :cond_f
    :goto_a
    const/4 v2, 0x0

    .line 298
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 299
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 300
    invoke-virtual {v3, v4, v6}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 301
    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 302
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dolby/daxappui/DAXApplication;->getIeqViewWidth()F

    move-result v4

    float-to-int v4, v4

    mul-int/2addr v4, v7

    .line 303
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    mul-int/2addr v5, v7

    add-int/2addr v4, v5

    .line 304
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0601c5    # @dimen/profile_switch_track_width '46.0dp'

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 305
    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v4, 0x7f090064    # @id/deButton

    .line 307
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    .line 308
    invoke-virtual {v4, v2}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 309
    sget-object v2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v2, v5}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 311
    sget-object v2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v5, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v2, v5}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    const/4 v2, 0x0

    goto :goto_b

    :cond_10
    const/4 v2, 0x0

    .line 313
    invoke-virtual {v4, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 315
    :goto_b
    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 316
    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const/16 v4, 0x8

    goto :goto_c

    :cond_11
    const/4 v2, 0x0

    const/16 v4, 0x8

    .line 321
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :goto_c
    const v3, 0x7f090149    # @id/profileDescription

    .line 325
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-nez v8, :cond_12

    .line 328
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_d

    .line 330
    :cond_12
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_d
    const v2, 0x7f0900ac    # @id/geqView

    .line 334
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/dolby/daxappui/GeqView;

    if-eqz v2, :cond_13

    .line 335
    iget v3, v2, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_13

    .line 336
    invoke-virtual {v2}, Lcom/dolby/daxappui/GeqView;->onUpdateGeqData()V

    .line 339
    :cond_13
    sget-object v2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v3, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-interface {v3}, Lcom/dolby/daxappui/IDsFragObserver;->getActivePort()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dolby/daxappui/DsClientSettings;->getGraphicEqualizerOn(Lcom/dolby/daxappui/IDsFragObserver;I)Z

    move-result v2

    const v3, 0x7f0900a9    # @id/geqText

    .line 340
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v2, :cond_15

    .line 342
    iget-object v2, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_14

    .line 344
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_14
    if-eqz v3, :cond_17

    .line 347
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f050057    # @color/colorText '@android:color/primary_text_light'

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_e

    :cond_15
    const v2, 0x7f0900a8    # @id/geqLayout

    .line 350
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 351
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    if-nez v4, :cond_16

    .line 353
    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    :cond_16
    if-eqz v3, :cond_17

    .line 356
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f050053    # @color/colorSwitchDisableText '@android:color/primary_text_light'

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_17
    :goto_e
    const v2, 0x7f09014e    # @id/profileResetButton

    .line 361
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v2, :cond_19

    .line 363
    sget-object v3, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v4, v0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v3, v4, v8}, Lcom/dolby/daxappui/DsClientSettings;->isProfileSpecificSettingsModified(Lcom/dolby/daxappui/IDsFragObserver;I)Z

    move-result v3

    if-eqz v3, :cond_18

    const/4 v3, 0x0

    .line 365
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_f

    :cond_18
    const/4 v3, 0x4

    .line 367
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 369
    :goto_f
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    :cond_19
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1a
    :goto_10
    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    .line 498
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .line 488
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 489
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 492
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onPresetChanged(IZ)V
    .locals 4

    .line 63
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 66
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0900d5    # @id/ieqName

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 68
    iget-object v1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0900d2    # @id/icon_off

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 70
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f09008d    # @id/equalizerListOffLayout

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    .line 73
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v3, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f07007e    # @drawable/ic_none_on_ieq 'res/drawable/ic_none_on_ieq.xml'

    .line 74
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 76
    :cond_1
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v3, 0x7f070061    # @drawable/background_with_shadow 'res/drawable/background_with_shadow.xml'

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f07007d    # @drawable/ic_none_off_ieq 'res/drawable/ic_none_off_ieq.xml'

    .line 77
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    if-eqz p2, :cond_2

    .line 80
    sget-object p2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {p2, p0, p1}, Lcom/dolby/daxappui/DsClientSettings;->setIeqPreset(Lcom/dolby/daxappui/IDsFragObserver;I)V

    :cond_2
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    if-eqz p3, :cond_3

    .line 504
    sget-object p1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object p3, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {p1, p3}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;)Z

    move-result p1

    const/4 p3, 0x1

    if-nez p2, :cond_2

    if-eqz p1, :cond_2

    .line 506
    sget-object p1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object p2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/dolby/daxappui/DsClientSettings;->setDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;Z)V

    .line 507
    iget-object p1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const-string p2, "dax_dea_default"

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const/4 p2, 0x0

    .line 509
    iget v1, p0, Lcom/dolby/daxappui/FragProfilePanel;->mNum:I

    if-ne v1, p3, :cond_0

    const-string p2, "DialogEnhancerAmountForMovie"

    goto :goto_0

    :cond_0
    const/4 p3, 0x3

    if-ne v1, p3, :cond_1

    const-string p2, "DialogEnhancerAmountForCustom"

    :cond_1
    :goto_0
    if-eqz p2, :cond_3

    .line 516
    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    .line 517
    sget-object p2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {p2, p0, p1}, Lcom/dolby/daxappui/DsClientSettings;->setDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;I)V

    goto :goto_1

    .line 520
    :cond_2
    sget-object p1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {p1, v0, p3}, Lcom/dolby/daxappui/DsClientSettings;->setDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;Z)V

    .line 521
    sget-object p1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object p0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {p1, p0, p2}, Lcom/dolby/daxappui/DsClientSettings;->setDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;I)V

    :cond_3
    :goto_1
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public setGeqViewEnabled()V
    .locals 4

    .line 86
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    if-eqz v0, :cond_4

    .line 87
    sget-object v1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    invoke-interface {v0}, Lcom/dolby/daxappui/IDsFragObserver;->getActivePort()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/dolby/daxappui/DsClientSettings;->getGraphicEqualizerOn(Lcom/dolby/daxappui/IDsFragObserver;I)Z

    move-result v0

    .line 88
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0900a9    # @id/geqText

    if-eqz v0, :cond_1

    if-eqz v1, :cond_3

    .line 91
    iget-object v0, p0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 93
    iget-object v3, p0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 95
    :cond_0
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 97
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050057    # @color/colorText '@android:color/primary_text_light'

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_3

    const v0, 0x7f090006    # @id/GeqViewLayout

    .line 102
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dolby/daxappui/GeqViewLayout;

    .line 103
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 104
    iget-object v3, p0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-nez v3, :cond_2

    .line 106
    iget-object v3, p0, Lcom/dolby/daxappui/FragProfilePanel;->mMask:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_2
    if-eqz v2, :cond_3

    .line 109
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f050033    # @color/colorGeqDisableText '@android:color/primary_text_light'

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {v0, v3, p0}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p0

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    :goto_0
    if-eqz v1, :cond_4

    const p0, 0x7f0900ac    # @id/geqView

    .line 114
    invoke-virtual {v1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/dolby/daxappui/GeqView;

    .line 115
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_4
    return-void
.end method

.method public updateProfilePanel(I)V
    .locals 9

    .line 401
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 404
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040004    # @bool/tabletLayout 'false'

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f09014b    # @id/profileIcon

    .line 406
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 407
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->profileImgId_dax2:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    const v1, 0x7f09014d    # @id/profileName

    .line 410
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 411
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mProfileNames:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0900d4    # @id/ieqLayout

    .line 414
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/16 v4, 0x8

    const/4 v5, 0x0

    if-eq p1, v3, :cond_2

    if-ne p1, v2, :cond_1

    goto :goto_0

    .line 433
    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_2

    .line 416
    :cond_2
    :goto_0
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 417
    sget-object v1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v6, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v1, v6}, Lcom/dolby/daxappui/DsClientSettings;->getIeqPreset(Lcom/dolby/daxappui/IDsFragObserver;)I

    move-result v1

    const v6, 0x7f0900d5    # @id/ieqName

    .line 418
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 419
    iget-object v7, p0, Lcom/dolby/daxappui/FragProfilePanel;->mIeqName:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f0900d2    # @id/icon_off

    .line 420
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    const v7, 0x7f09008d    # @id/equalizerListOffLayout

    .line 421
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    if-ne v1, v2, :cond_3

    .line 423
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v8, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v2, v8}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f07007e    # @drawable/ic_none_on_ieq 'res/drawable/ic_none_on_ieq.xml'

    .line 424
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 426
    :cond_3
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mContext:Landroid/content/Context;

    const v8, 0x7f070061    # @drawable/background_with_shadow 'res/drawable/background_with_shadow.xml'

    invoke-static {v2, v8}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f07007d    # @drawable/ic_none_off_ieq 'res/drawable/ic_none_off_ieq.xml'

    .line 427
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 429
    :goto_1
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mEqualizerAdapter:Lcom/dolby/daxappui/EqualizerAdapter;

    if-eqz v2, :cond_4

    .line 430
    invoke-virtual {v2, v1, v5}, Lcom/dolby/daxappui/EqualizerAdapter;->setIeqSelection(IZ)V

    :cond_4
    :goto_2
    const v1, 0x7f090069    # @id/deView

    .line 437
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 438
    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mProductVersion:Ljava/lang/String;

    const/4 v6, 0x4

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v7, "DAX3"

    .line 439
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz p1, :cond_7

    if-ne p1, v3, :cond_5

    goto :goto_3

    .line 443
    :cond_5
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const v1, 0x7f090064    # @id/deButton

    .line 444
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    .line 445
    sget-object v2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v3, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v2, v3}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerOn(Lcom/dolby/daxappui/IDsFragObserver;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 447
    sget-object v2, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v3, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v2, v3}, Lcom/dolby/daxappui/DsClientSettings;->getDialogEnhancerAmount(Lcom/dolby/daxappui/IDsFragObserver;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_4

    .line 449
    :cond_6
    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_4

    .line 441
    :cond_7
    :goto_3
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_8
    :goto_4
    const v1, 0x7f090149    # @id/profileDescription

    .line 455
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_a

    if-nez p1, :cond_9

    .line 459
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 461
    :cond_9
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_a
    :goto_5
    const v1, 0x7f0900ac    # @id/geqView

    .line 466
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/dolby/daxappui/GeqView;

    if-eqz v1, :cond_b

    .line 467
    iget v2, v1, Lcom/dolby/daxappui/GeqView;->mSelectedBar:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_b

    .line 468
    invoke-virtual {v1}, Lcom/dolby/daxappui/GeqView;->onUpdateGeqData()V

    :cond_b
    const v1, 0x7f09014e    # @id/profileResetButton

    .line 472
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    .line 474
    sget-object v1, Lcom/dolby/daxappui/DsClientSettings;->INSTANCE:Lcom/dolby/daxappui/DsClientSettings;

    iget-object v2, p0, Lcom/dolby/daxappui/FragProfilePanel;->mFObserver:Lcom/dolby/daxappui/IDsFragObserver;

    invoke-virtual {v1, v2, p1}, Lcom/dolby/daxappui/DsClientSettings;->isProfileSpecificSettingsModified(Lcom/dolby/daxappui/IDsFragObserver;I)Z

    move-result p1

    if-eqz p1, :cond_c

    .line 476
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_6

    .line 478
    :cond_c
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 482
    :cond_d
    :goto_6
    invoke-virtual {p0}, Lcom/dolby/daxappui/FragProfilePanel;->setGeqViewEnabled()V

    :cond_e
    return-void
.end method
