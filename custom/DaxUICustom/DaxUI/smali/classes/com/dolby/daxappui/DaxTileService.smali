.class public Lcom/dolby/daxappui/DaxTileService;
.super Landroid/service/quicksettings/TileService;
.source "DaxTileService.java"


# static fields
.field private static mPriority:I


# instance fields
.field private mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const/4 v0, -0x1

    sput v0, Lcom/dolby/daxappui/DaxTileService;->mPriority:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Landroid/service/quicksettings/TileService;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    return-void
.end method

.method private getDolbyState()I
    .locals 1

    .line 59
    nop

    .line 60
    iget-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_0

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDsOn()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 67
    :cond_0
    const/4 v0, 0x2

    :goto_0
    return v0
.end method

.method private getDolbyVersion()Ljava/lang/String;
    .locals 2

    .line 88
    const-string v0, "DAX3"

    .line 89
    iget-object v1, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v1, :cond_0

    .line 91
    :try_start_0
    iget-object v1, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->getDsVersion()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    nop

    .line 96
    move-object v0, v1

    goto :goto_0

    .line 92
    :catch_0
    move-exception v1

    .line 93
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 96
    :cond_0
    :goto_0
    return-object v0
.end method

.method private setDolbyState()V
    .locals 4

    .line 71
    iget-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->getDsOn()Z

    move-result v0

    .line 74
    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->hasControl()Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->release()V

    .line 76
    const/4 v1, 0x0

    sput v1, Lcom/dolby/daxappui/DaxTileService;->mPriority:I

    .line 77
    new-instance v2, Lcom/dolby/dax/DolbyAudioEffect;

    sget v3, Lcom/dolby/daxappui/DaxTileService;->mPriority:I

    invoke-direct {v2, v3, v1}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    iput-object v2, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1, v0}, Lcom/dolby/dax/DolbyAudioEffect;->setDsOn(Z)V

    .line 80
    invoke-direct {p0}, Lcom/dolby/daxappui/DaxTileService;->updateDolbyTileUI()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 85
    :cond_1
    :goto_0
    return-void
.end method

.method private updateDolbyTileUI()V
    .locals 5

    .line 32
    invoke-virtual {p0}, Lcom/dolby/daxappui/DaxTileService;->getQsTile()Landroid/service/quicksettings/Tile;

    move-result-object v0

    .line 33
    invoke-direct {p0}, Lcom/dolby/daxappui/DaxTileService;->getDolbyState()I

    move-result v1

    .line 34
    const-string v2, "DaxTileService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateDolbyTileUI, daxState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-direct {p0}, Lcom/dolby/daxappui/DaxTileService;->getDolbyVersion()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 36
    const/4 v3, 0x1

    packed-switch v1, :pswitch_data_0

    .line 45
    invoke-virtual {v0, v3}, Landroid/service/quicksettings/Tile;->setState(I)V

    goto :goto_0

    .line 42
    :pswitch_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/service/quicksettings/Tile;->setState(I)V

    .line 43
    goto :goto_0

    .line 39
    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/service/quicksettings/Tile;->setState(I)V

    .line 40
    nop

    .line 49
    :goto_0
    const-string v1, "DS1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const v1, 0x7f100029    # @string/app_name_ds1 'Dolby Audio'

    invoke-virtual {p0, v1}, Lcom/dolby/daxappui/DaxTileService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/service/quicksettings/Tile;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 52
    :cond_0
    const v1, 0x7f100028    # @string/app_name 'Dolby Atmos'

    invoke-virtual {p0, v1}, Lcom/dolby/daxappui/DaxTileService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/service/quicksettings/Tile;->setLabel(Ljava/lang/CharSequence;)V

    .line 55
    :goto_1
    invoke-virtual {v0}, Landroid/service/quicksettings/Tile;->updateTile()V

    .line 56
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onClick()V
    .locals 2

    .line 114
    invoke-super {p0}, Landroid/service/quicksettings/TileService;->onClick()V

    .line 115
    invoke-direct {p0}, Lcom/dolby/daxappui/DaxTileService;->setDolbyState()V

    .line 116
    const-string v0, "DaxTileService"

    const-string v1, "onClick"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    return-void
.end method

.method public onCreate()V
    .locals 3

    .line 131
    invoke-super {p0}, Landroid/service/quicksettings/TileService;->onCreate()V

    .line 132
    new-instance v0, Lcom/dolby/dax/DolbyAudioEffect;

    sget v1, Lcom/dolby/daxappui/DaxTileService;->mPriority:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    iput-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 133
    const-string v0, "DaxTileService"

    const-string v1, "onCreate() executed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 138
    invoke-super {p0}, Landroid/service/quicksettings/TileService;->onDestroy()V

    .line 139
    iget-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->release()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dolby/daxappui/DaxTileService;->mDolbyAudio:Lcom/dolby/dax/DolbyAudioEffect;

    .line 143
    :cond_0
    const-string v0, "DaxTileService"

    const-string v1, "onDestory() executed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return-void
.end method

.method public onStartListening()V
    .locals 2

    .line 101
    invoke-super {p0}, Landroid/service/quicksettings/TileService;->onStartListening()V

    .line 102
    invoke-direct {p0}, Lcom/dolby/daxappui/DaxTileService;->updateDolbyTileUI()V

    .line 103
    const-string v0, "DaxTileService"

    const-string v1, "onStartListening"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    return-void
.end method

.method public onStopListening()V
    .locals 2

    .line 108
    invoke-super {p0}, Landroid/service/quicksettings/TileService;->onStopListening()V

    .line 109
    const-string v0, "DaxTileService"

    const-string v1, "onStopListening"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    return-void
.end method

.method public onTileAdded()V
    .locals 2

    .line 121
    const-string v0, "DaxTileService"

    const-string v1, "onTileAdded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    return-void
.end method

.method public onTileRemoved()V
    .locals 2

    .line 126
    const-string v0, "DaxTileService"

    const-string v1, "onTileRemoved"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void
.end method
