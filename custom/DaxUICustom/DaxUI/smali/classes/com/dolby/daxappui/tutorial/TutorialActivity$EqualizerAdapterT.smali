.class Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;
.super Landroid/widget/BaseAdapter;
.source "TutorialActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dolby/daxappui/tutorial/TutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EqualizerAdapterT"
.end annotation


# instance fields
.field private imgIdForIeqOff:[I

.field private imgIdForIeqOn:[I

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;


# direct methods
.method constructor <init>(Lcom/dolby/daxappui/tutorial/TutorialActivity;Landroid/content/Context;)V
    .locals 1

    .line 677
    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->this$0:Lcom/dolby/daxappui/tutorial/TutorialActivity;

    .line 678
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 p1, 0x3

    new-array v0, p1, [I

    .line 665
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->imgIdForIeqOn:[I

    new-array p1, p1, [I

    .line 671
    fill-array-data p1, :array_1

    iput-object p1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->imgIdForIeqOff:[I

    .line 679
    iput-object p2, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->mContext:Landroid/content/Context;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f070073    # @drawable/ic_focus_on_ieq 'res/drawable/ic_focus_on_ieq.xml'
        0x7f070080    # @drawable/ic_open_on_ieq 'res/drawable/ic_open_on_ieq.xml'
        0x7f070085    # @drawable/ic_rich_on_ieq 'res/drawable/ic_rich_on_ieq.xml'
    .end array-data

    :array_1
    .array-data 4
        0x7f070072    # @drawable/ic_focus_off_ieq 'res/drawable/ic_focus_off_ieq.xml'
        0x7f07007f    # @drawable/ic_open_off_ieq 'res/drawable/ic_open_off_ieq.xml'
        0x7f070084    # @drawable/ic_rich_off_ieq 'res/drawable/ic_rich_off_ieq.xml'
    .end array-data
.end method


# virtual methods
.method public getCount()I
    .locals 0

    .line 684
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->imgIdForIeqOn:[I

    array-length p0, p0

    return p0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    .line 694
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    .line 700
    iget-object p2, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const p3, 0x7f0c002d    # @layout/equalizer_list_item 'res/layout/equalizer_list_item.xml'

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f0900d0    # @id/icon

    .line 703
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    const v0, 0x7f090091    # @id/equalizerListViewLayout

    .line 704
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 706
    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->mContext:Landroid/content/Context;

    const v2, 0x7f070060    # @drawable/background_with_blue_shadow 'res/drawable/background_with_blue_shadow.xml'

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 707
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->imgIdForIeqOn:[I

    aget p0, p0, p1

    invoke-virtual {p3, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 709
    :cond_1
    iget-object v1, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->mContext:Landroid/content/Context;

    const v2, 0x7f070061    # @drawable/background_with_shadow 'res/drawable/background_with_shadow.xml'

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 710
    iget-object p0, p0, Lcom/dolby/daxappui/tutorial/TutorialActivity$EqualizerAdapterT;->imgIdForIeqOff:[I

    aget p0, p0, p1

    invoke-virtual {p3, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-object p2
.end method
