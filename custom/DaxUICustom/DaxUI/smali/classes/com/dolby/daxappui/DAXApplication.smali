.class public Lcom/dolby/daxappui/DAXApplication;
.super Landroid/app/Application;
.source "DAXApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dolby/daxappui/DAXApplication$ConfigurationWrapper;
    }
.end annotation


# static fields
.field static mBaseDensity:I

.field static mDAXApplication:Lcom/dolby/daxappui/DAXApplication;

.field static mDefaultDensity:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 32
    sput-object p0, Lcom/dolby/daxappui/DAXApplication;->mDAXApplication:Lcom/dolby/daxappui/DAXApplication;

    return-void
.end method

.method public static getInstance()Lcom/dolby/daxappui/DAXApplication;
    .locals 1

    .line 36
    sget-object v0, Lcom/dolby/daxappui/DAXApplication;->mDAXApplication:Lcom/dolby/daxappui/DAXApplication;

    return-object v0
.end method


# virtual methods
.method public getIeqViewWidth()F
    .locals 5

    const-string v0, "window"

    .line 107
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 109
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 110
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 111
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 112
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 114
    sget v1, Lcom/dolby/daxappui/DAXApplication;->mBaseDensity:I

    int-to-float v1, v1

    sget v2, Lcom/dolby/daxappui/DAXApplication;->mDefaultDensity:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float v0, v0

    .line 116
    invoke-virtual {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06009a    # @dimen/ieq_margin_start '64.0dp'

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 117
    invoke-virtual {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060099    # @dimen/ieq_margin_end '12.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 118
    invoke-virtual {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060097    # @dimen/ieq_horizontal_spacing_normal '2.0dp'

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 119
    invoke-virtual {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v3, 0x7f060098    # @dimen/ieq_horizontal_spacing_off '8.0dp'

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p0

    mul-int/lit8 p0, p0, 0x3

    add-int/2addr v2, p0

    int-to-float p0, v2

    div-float/2addr p0, v1

    sub-float/2addr v0, p0

    const/high16 p0, 0x40800000    # 4.0f

    div-float/2addr v0, p0

    return v0
.end method

.method public getProductVersion()Ljava/lang/String;
    .locals 2

    .line 42
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v0, "com.dolby.daxappui"

    const/16 v1, 0x4000

    invoke-virtual {p0, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    .line 43
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 45
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public getProfileNames()[Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const v1, 0x7f10003b    # @string/dynamic 'Dynamic'

    .line 52
    invoke-virtual {p0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f10007a    # @string/movie 'Movie'

    .line 53
    invoke-virtual {p0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const v1, 0x7f10007c    # @string/music 'Music'

    .line 54
    invoke-virtual {p0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const v1, 0x7f100031    # @string/custom 'Custom'

    .line 55
    invoke-virtual {p0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x3

    aput-object p0, v0, v1

    return-object v0
.end method
