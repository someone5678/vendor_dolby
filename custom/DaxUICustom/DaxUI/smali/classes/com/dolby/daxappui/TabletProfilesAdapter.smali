.class Lcom/dolby/daxappui/TabletProfilesAdapter;
.super Landroid/widget/BaseAdapter;
.source "TabletProfilesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProfileNames:[Ljava/lang/String;

.field private final mProfiles:[Lcom/dolby/daxappui/Profile;

.field private mSelectedProfile:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 4

    .line 36
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x3

    .line 27
    iput v0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mSelectedProfile:I

    .line 37
    iput-object p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mContext:Landroid/content/Context;

    .line 38
    invoke-static {}, Lcom/dolby/daxappui/DAXApplication;->getInstance()Lcom/dolby/daxappui/DAXApplication;

    move-result-object p1

    invoke-virtual {p1}, Lcom/dolby/daxappui/DAXApplication;->getProfileNames()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfileNames:[Ljava/lang/String;

    const/4 p1, 0x4

    new-array p1, p1, [Lcom/dolby/daxappui/Profile;

    .line 39
    iput-object p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    .line 40
    iget-object p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    new-instance v1, Lcom/dolby/daxappui/Profile;

    const v2, 0x7f07006f    # @drawable/ic_dynamic_profile_panel 'res/drawable/ic_dynamic_profile_panel.xml'

    const v3, 0x7f070070    # @drawable/dummy_ae_70 'false'

    invoke-direct {v1, v2, v3}, Lcom/dolby/daxappui/Profile;-><init>(II)V

    const/4 v2, 0x0

    aput-object v1, p1, v2

    .line 41
    iget-object p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    new-instance v1, Lcom/dolby/daxappui/Profile;

    const v2, 0x7f070076    # @drawable/ic_movie_profile_panel 'res/drawable/ic_movie_profile_panel.xml'

    const v3, 0x7f070077    # @drawable/dummy_ae_77 'false'

    invoke-direct {v1, v2, v3}, Lcom/dolby/daxappui/Profile;-><init>(II)V

    const/4 v2, 0x1

    aput-object v1, p1, v2

    .line 42
    iget-object p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    new-instance v1, Lcom/dolby/daxappui/Profile;

    const v2, 0x7f07007b    # @drawable/ic_music_profile_panel 'res/drawable/ic_music_profile_panel.xml'

    const v3, 0x7f07007c    # @drawable/dummy_ae_7c 'false'

    invoke-direct {v1, v2, v3}, Lcom/dolby/daxappui/Profile;-><init>(II)V

    const/4 v2, 0x2

    aput-object v1, p1, v2

    .line 43
    iget-object p0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    new-instance p1, Lcom/dolby/daxappui/Profile;

    const v1, 0x7f07006d    # @drawable/ic_custom_profile_panel 'res/drawable/ic_custom_profile_panel.xml'

    const v2, 0x7f07006e    # @drawable/dummy_ae_6e 'false'

    invoke-direct {p1, v1, v2}, Lcom/dolby/daxappui/Profile;-><init>(II)V

    aput-object p1, p0, v0

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    array-length p0, p0

    return p0
.end method

.method public getItem(I)Lcom/dolby/daxappui/Profile;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    aget-object p0, p0, p1

    return-object p0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 22
    invoke-virtual {p0, p1}, Lcom/dolby/daxappui/TabletProfilesAdapter;->getItem(I)Lcom/dolby/daxappui/Profile;

    move-result-object p0

    return-object p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 62
    iget-object p2, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const p3, 0x7f0c0083    # @layout/dummy_ae_83 'false'

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 64
    new-instance p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;

    invoke-direct {p3, p0, v0}, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;-><init>(Lcom/dolby/daxappui/TabletProfilesAdapter;Lcom/dolby/daxappui/TabletProfilesAdapter$1;)V

    const v0, 0x7f09014b    # @id/profileIcon

    .line 65
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->imageView:Landroid/widget/ImageView;

    const v0, 0x7f09014d    # @id/profileName

    .line 66
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->title:Landroid/widget/TextView;

    .line 67
    iput p1, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->position:I

    .line 69
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;

    .line 74
    :goto_0
    iget-object v0, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfileNames:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mProfiles:[Lcom/dolby/daxappui/Profile;

    aget-object v0, v0, p1

    .line 76
    iget v1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mSelectedProfile:I

    if-ne p1, v1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 77
    :goto_1
    iget-object v1, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Lcom/dolby/daxappui/Profile;->getIcon(Z)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 80
    iget-object p1, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->title:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f050051    # @color/colorSelectedTabText '@android:color/primary_text_light'

    iget-object p0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {p3, v0, p0}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 83
    iget-object p1, p3, Lcom/dolby/daxappui/TabletProfilesAdapter$Holder;->title:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f05004a    # @color/colorProfileText '@android:color/primary_text_light'

    iget-object p0, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-virtual {p3, v0, p0}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_2
    return-object p2
.end method

.method setProfileSelected(I)V
    .locals 0

    .line 90
    iput p1, p0, Lcom/dolby/daxappui/TabletProfilesAdapter;->mSelectedProfile:I

    .line 91
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
