.class Lcom/dolby/daxappui/DAXConfiguration;
.super Ljava/lang/Object;
.source "DAXConfiguration.java"


# static fields
.field private static dynamicInstance:Lcom/dolby/daxappui/DAXConfiguration;


# instance fields
.field private maxEditGain:F

.field private minEditGain:F


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    const-string v0, "DAXConfiguration"

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, -0x3ec00000    # -12.0f

    .line 23
    iput v1, p0, Lcom/dolby/daxappui/DAXConfiguration;->minEditGain:F

    const/high16 v1, 0x41400000    # 12.0f

    .line 24
    iput v1, p0, Lcom/dolby/daxappui/DAXConfiguration;->maxEditGain:F

    const/high16 v1, 0x7fc00000    # Float.NaN

    .line 28
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100079    # @string/min_edit_gain '-12'

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/dolby/daxappui/DAXConfiguration;->minEditGain:F

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f100078    # @string/max_edit_gain '12'

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1

    iput p1, p0, Lcom/dolby/daxappui/DAXConfiguration;->maxEditGain:F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    iput v1, p0, Lcom/dolby/daxappui/DAXConfiguration;->minEditGain:F

    .line 36
    iput v1, p0, Lcom/dolby/daxappui/DAXConfiguration;->maxEditGain:F

    const-string p0, "Some of values from configuration.xml were not loaded!"

    .line 37
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 31
    :catch_1
    iput v1, p0, Lcom/dolby/daxappui/DAXConfiguration;->minEditGain:F

    .line 32
    iput v1, p0, Lcom/dolby/daxappui/DAXConfiguration;->maxEditGain:F

    const-string p0, "Some of values from configuration.xml were not float type!"

    .line 33
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static getInstance(Landroid/content/Context;)Lcom/dolby/daxappui/DAXConfiguration;
    .locals 1

    .line 47
    sget-object v0, Lcom/dolby/daxappui/DAXConfiguration;->dynamicInstance:Lcom/dolby/daxappui/DAXConfiguration;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/dolby/daxappui/DAXConfiguration;

    invoke-direct {v0, p0}, Lcom/dolby/daxappui/DAXConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dolby/daxappui/DAXConfiguration;->dynamicInstance:Lcom/dolby/daxappui/DAXConfiguration;

    .line 50
    :cond_0
    sget-object p0, Lcom/dolby/daxappui/DAXConfiguration;->dynamicInstance:Lcom/dolby/daxappui/DAXConfiguration;

    return-object p0
.end method


# virtual methods
.method getMaxEditGain()F
    .locals 1

    .line 54
    iget v0, p0, Lcom/dolby/daxappui/DAXConfiguration;->maxEditGain:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 p0, 0x41400000    # 12.0f

    return p0

    .line 57
    :cond_0
    iget p0, p0, Lcom/dolby/daxappui/DAXConfiguration;->maxEditGain:F

    return p0
.end method

.method getMinEditGain()F
    .locals 1

    .line 61
    iget v0, p0, Lcom/dolby/daxappui/DAXConfiguration;->minEditGain:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 p0, -0x3ec00000    # -12.0f

    return p0

    .line 64
    :cond_0
    iget p0, p0, Lcom/dolby/daxappui/DAXConfiguration;->minEditGain:F

    return p0
.end method
