.class public final Landroid/support/v17/leanback/R$string;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final lb_search_bar_hint:I = 0x7f100074 # @string/lb_search_bar_hint 'Search'

.field public static final lb_search_bar_hint_speech:I = 0x7f100075 # @string/lb_search_bar_hint_speech 'Speak to search'

.field public static final lb_search_bar_hint_with_title:I = 0x7f100076 # @string/lb_search_bar_hint_with_title 'Search %1$s'

.field public static final lb_search_bar_hint_with_title_speech:I = 0x7f100077 # @string/lb_search_bar_hint_with_title_speech 'Speak to search %1$s'
