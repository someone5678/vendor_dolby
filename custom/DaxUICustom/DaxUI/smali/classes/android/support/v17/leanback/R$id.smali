.class public final Landroid/support/v17/leanback/R$id;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final bar1:I = 0x7f090034 # @id/bar1

.field public static final bar2:I = 0x7f090035 # @id/bar2

.field public static final bar3:I = 0x7f090036 # @id/bar3

.field public static final guidance_breadcrumb:I = 0x7f0900b2 # @id/guidance_breadcrumb

.field public static final guidance_description:I = 0x7f0900b4 # @id/guidance_description

.field public static final guidance_icon:I = 0x7f0900b5 # @id/guidance_icon

.field public static final guidance_title:I = 0x7f0900b6 # @id/guidance_title

.field public static final guidedactions_sub_list:I = 0x7f0900c6 # @id/guidedactions_sub_list

.field public static final icon:I = 0x7f0900d0 # @id/icon

.field public static final lb_parallax_source:I = 0x7f0900fa # @id/lb_parallax_source

.field public static final lb_search_bar_badge:I = 0x7f0900fe # @id/lb_search_bar_badge

.field public static final lb_search_bar_items:I = 0x7f0900ff # @id/lb_search_bar_items

.field public static final lb_search_bar_speech_orb:I = 0x7f090100 # @id/lb_search_bar_speech_orb

.field public static final lb_search_text_editor:I = 0x7f090102 # @id/lb_search_text_editor

.field public static final lb_slide_transition_value:I = 0x7f090106 # @id/lb_slide_transition_value

.field public static final picker:I = 0x7f09013c # @id/picker

.field public static final playback_progress:I = 0x7f090141 # @id/playback_progress

.field public static final search_orb:I = 0x7f090171 # @id/search_orb

.field public static final title_badge:I = 0x7f0901a0 # @id/title_badge

.field public static final title_orb:I = 0x7f0901a1 # @id/title_orb

.field public static final title_text:I = 0x7f0901a3 # @id/title_text

.field public static final transitionPosition:I = 0x7f0901a9 # @id/transitionPosition
